let arr = [1, 2, 3];
arr[0];
//let object = { name: 'dong phuoc duc', age: 20, fly() { return "I can fly" } };
let object = { name: 'dong phuoc duc', age: 20 }
//let  name=object.name;
//let age=object,age;
//let object = { name, age };
let object2 = Object.assign({}, object);
//Checks method
console.log('name' in object);
console.log(object.name !== undefined);
console.log(object.hasOwnProperty('name'));
//get keys
Object.keys(object)//.forEach;
//get values
console.log(Object.values(object));

let object3 ={
    name :"dong phuoc duc",
    age : 18,
    birthday: {day:12, month:1, year:1999},
    score:[9,8,7,6],
    fly(){
        console.log(`${this.name} can fly`);
    }
};


object3.fly()
// convert object to JSON
console.log(JSON.stringify(object3));
//convert JSON  to object
object3=JSON.parse(json);