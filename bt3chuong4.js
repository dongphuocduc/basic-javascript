function nonrepeat(str) {
    // let arr=str.split('')
    // let temp;
    // arr.forEach(element => {
    //     if(arr.indexOf(element)==arr.lastIndexOf(element))
    //         temp=element;
    // });
    // return temp;
    return str.split('')
        .map(element => str.indexOf(element) == str.lastIndexOf(element) ? element : null)
        .filter(_ => _)[0]
}
console.log(nonrepeat('aabbcelcdd'));
