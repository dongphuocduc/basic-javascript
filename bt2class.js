class Personal {
    constructor(ID, fullName, email, birthday, position, seniority) {
        this.ID = ID;
        this.fullName = fullName;
        this.email = email;
        this.birthday = birthday;
        this.position = position;
        this.seniority = seniority;
    }
    get Age() {
        let age = 0;
        let current = new Date();
        age = current.getFullYear() - this.birthday.year
        if (this.birthday.month > current.getMonth() + 1) {
            age--;
        }
        else if (this.birthday.month == current.getMonth() + 1 && this.birthday.day > current.getDate()) {
            age--;
        }
        return age;
    }
    get Salary() {
        let basicSalary = {
            manager: 1000,
            secterary: 400,
            employee: 300
        }
        let salaryPosition = basicSalary[this.position]
        if (this.seniority < 12) {
            return basicSalary * 1.2
        }
        else if (this.seniority < 24) {
            return basicSalary * 2
        }
        else if (this.seniority < 36) {
            return basicSalary * 3.4
        }
        else if (this.seniority > 36) {
            return basicSalary * 4.5
        }
    }
    static personalHighest(personal){
        let arr=[];
        personal=personal.sort((slr1,slr2)=>slr1.Salary<slr2.Salary)
        personal.forEach(element => {
            if(personal[0].Salary == element.Salary){
                arr.push(element)
            }
        });
    }
}
let Personal1 = [new Personal(001, "Dao Minh Chau", "daominhchau18@gmail.com", { date: 24, month: 01, year: 1997 }, "director", 24),
new Personal(002, "Tran Van Sinh", "tranvansinh@gmail.com", { date: 24, month: 05, year: 1999 }, "assistant", 14),
new Personal(003, "Dong Phuoc Duc", "dongphuocducdn1999@gmail.com", { date: 12, month: 01, year: 1999 }, "manager", 37),
new Personal(004, "Nguyen Van Cam", "camnguyen@gmail.com", { date: 16, month: 05, year: 1999 }, "secterary", 10),
new Personal(005, "Dang Mau Quang", "mauquang1999@gmail.com", { date: 14, month: 09, year: 1999 }, "employee", 22)]

//console.log(employee);
Personal1.forEach(element => {
    console.log(element);
    console.log(`Age: ${element.Age}`);
    console.log(`Salary: ${element.Salary
    }`);
    
});
