class Animal{
    constructor(name,age,spec){
        this.name=name;
        this.age=age;
        this.spec=spec;
    }
    speak(){
        return"meo meo";
    }
    get getAge(){
        return this.age;
    }
    static sumCatAge(cat1,cat2){
        return cat1.age+ cat2.age;
    }
}
class Dog extends Animal{
    constructor(name,age,gender,spec="dog"){
        super(name,age,spec)
        this.gender=gender;
    }
    speak(){
        return "gau gau"
    }
}
let cat1=new Animal("paw",2,"cat");
let cat2=new Animal("pew",3,"cat");
let dog1 = new Dog("milu",5,"male");
console.log(cat1);
console.log(dog1);
console.log(dog1.hasOwnProperty("name"));


//check
cat1 instanceof Animal
//call method speak()
console.log(cat1.speak());
console.log(cat1.getAge);
console.log(Animal.sumCatAge(cat1,cat2));
Animal.prototype.color = "black";
console.log(cat1.hasOwnProperty("color"));




