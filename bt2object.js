let scores = [
    {
        studentName: "Dong Phuoc Duc",
        math: 6.0,
        physical: 7.25,
        chemistry: 5.25,
        history: 5.5,
        geography: 5.75,
        literature: 6.5,
    },
   {
        studentName: "Dao Minh Chau",
        math: 6.25,
        physical: 7.0,
        chemistry: 6.25,
        history: 6.5,
        geography: 5.5,
        literature: 6.5,
    },
    {
        studentName: "Tran Van Sinh",
        math: 6.5,
        physical: 7.5,
        chemistry: 6.75,
        history: 6.0,
        geography: 6.5,
        literature: 6.5,
    },
]
let max=0;
let temp
scores.forEach((element,index) => {
    let average=(element.chemistry+element.geography+element.literature+element.physical+element.math+element.history)/6;
    if(average>max){
        max=average;
        temp=index;
    }
});
console.log(scores[temp].studentName,max);
console.log(scores[temp].physical);

