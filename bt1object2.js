let bill =
    [
        {
            ID: 1,
            content: [{ productID: 325, amount: 2 }, { productID: 612, amount: 6 }, { productID: 123, amount: 10 }],
            totalPrice: 0
        },
        {
            ID: 2,
            content: [{ productID: 601, amount: 1 }, { productID: 123, amount: 2 }, { productID: 325, amount: 3 }],
            totalPrice: 0
        },
    ]

let product = [
    {
        ID: 123,
        name: "The New iPad 2018",
        price: 799,
        brand: "Apple",
        classify: "Tablet",
    },
    {
        ID: 325,
        name: "Mi Mix 3",
        price: 499,
        brand: "Xiaomi",
        classify: "Smartphone",
    },
    {
        ID: 612,
        name: "Alienware 17R5",
        price: 1549,
        brand: "Dell",
        classify: "Laptop",
    },
    {
        ID: 601,
        name: "Macbook Pro 2018",
        price: 1299,
        brand: "Apple",
        classify: "Laptop",
    }
]
//bai 2
function findBrand(brand) {
    let arr = [];
    for (i = 0; i < product.length; i++)
        if (product[i].brand === brand) {
            arr.push(product[i])
        }
    return arr;
}
console.log(findBrand("Apple"));

//bai 1
bill.forEach((element1, index) => {
    element1.content.forEach((element2) => {
        product.forEach((element3) => {
            if (element2.productID === element3.ID) {
                bill[index].totalPrice += element2.amount * element3.price;
            }
        });
    });
    //console.log(element1);
});

//bai 3
function displayBill(totalbill) {
    let str = '';
    totalbill.content.forEach((element4, index1) => {
        let sum = 0;
        product.forEach(element5 => {
            if (element4.productID === element5.ID) {
                sum += element4.amount * element5.price;
                str += `${index1 + 1}. ${element5.brand} ${element5.name} 
                        x${element4.amount}: ${sum} USD \n`;
            }
        });
        bill.totalPrice += sum;
    });
    str += `\tTotal Price: ${totalbill.totalPrice} USD`
    return str;
}
console.log(displayBill(bill[0]));


