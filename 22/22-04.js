class Customer {
    constructor(Name, Member, Type) {
        this.Name = Name;
        this.Member = Member;
        this.Type = Type;
    }
}
class DiscountRate {
    constructor() {
        this.ServiceDiscount = { Premium: 0.2, Gold: 0.15, Silver: 0.1 }
        this.ProductDiscount = { Premium: 0.1, Gold: 0.1, Silver: 0.1 }
    }
    static ServiceDiscount() {
        let rate = this.ServiceDiscount[Type];
        return rate !== undefined ? rate : 0
    }
    static ProductDiscount() {
        let rate = this.ProductDiscount[Type];
        return rate !== undefined ? rate : 0
    }
}
class Visit {
    constructor(customer, dateVisit) {
        this.customer = customer;
        this.dateVisit = dateVisit;
        this.serviceExpense = 0;
        this.productExpense = 0;
    }
    setServiceExpense(money) {
        if (this.customer.Member == false)
            this.serviceExpense = money
        else {
            this.serviceExpense = money - money * ServiceDiscount(this.customer.Type)
        }
    }
    setProductExpense(money) {
        if (this.customer.Member == false)
            this.productExpense = money;
        else {
            this.productExpense = money - money * ProductDiscount(this.customer.Type)
        }
    }
    get totalExpense() {
        return this.serviceExpense + this.productExpense;
    }
}