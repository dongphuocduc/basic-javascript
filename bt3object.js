let customer = {
    firstName: "John",
    lastName: "Smith",
    dateOfBirth: { day: 6, month: 2, year: 1998 },
    gender: "male",
    phoneNumber: "0775842390",
    email: "john_smith@gmail.com",
    address: "13th Street 47W, New York City"
};
let arr= Object.entries(customer);
let arrObject = [];
for(let i=0;i<arr.length;i++){
    let obj={};
    if(arr[i][0]==='dateofBirth')
    obj={title: arr[i][0], value: 
            `${arr[i][1].day}/${arr[i][1].month}/${arr[i][1.].year}`};
    else
    obj= {title: arr[i][0],value: arr[i][1]};
    arrObject.push(obj);    
}
console.log(JSON.stringify(arrObject));
//c2
let result = Object.keys(customer).map(key =>{
    if (key ==='dateOfBirth') 
    return {title: key,value:`${customer[key].month}/${customer[key].year}`};
});
console.log(result);
